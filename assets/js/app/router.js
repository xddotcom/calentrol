
/********************************** Router **********************************/

CLTApp.history = {
	active: CLTApp.Pages.Home,
	stack: []
};

CLTApp.Router = new (Backbone.Router.extend({
	initialize: function(){
		this.route('', 'index');
		this.route('search', 'search');
		this.route(/^card\/(\d+)$/, 'cardDetail');
		this.route('profile', 'profile');
	},
	
	index: function() { CLTApp.Pages.Home.go(); CLTApp.history.active = CLTApp.Pages.Home; },
	
	search: function() { CLTApp.Pages.Search.go(); CLTApp.history.active = CLTApp.Pages.Search; },
	cardDetail: function(cid) { CLTApp.Pages.CardDetail.go({cardId: cid}); CLTApp.history.active = CLTApp.Pages.CardDetail; },
	
	profile: function() { CLTApp.Pages.Profile.go(); CLTApp.history.active = CLTApp.Pages.Profile; },
	
}));

CLTApp.goTo = function(pageName, options) {
	var next = CLTApp.Pages[pageName];
	(options || (options = {})).caller = options.caller || CLTApp.history.active;
	if (next != CLTApp.history.active) {
		CLTApp.history.stack.push(CLTApp.history.active);
		CLTApp.history.active = next;
		CLTApp.history.active.go(options);
	}
	if (pageName == 'Home') CLTApp.history.stack.length = 0;
};

CLTApp.refreshActivePage = function() {
	CLTApp.history.active.refresh();
};

CLTApp.goBack = function() {
	if (CLTApp.history.stack.length > 0) {
		var prev = CLTApp.history.stack.pop();
		CLTApp.history.active = prev;
		CLTApp.history.active.showPage();
	} else if (CLTApp.history.active != CLTApp.Pages.Home) {
		CLTApp.history.active = CLTApp.Pages.Home;
		CLTApp.Pages.Home.go();
	}
};
