
CLTApp.Models.Coupon = CLTApp.Model.extend({});
CLTApp.Collections.Coupons = CLTApp.Collection.extend({
    model: CLTApp.Models.Coupon
});

CLTApp.Models.Badge = CLTApp.Model.extend({});
CLTApp.Collections.Badges = CLTApp.Collection.extend({
    model: CLTApp.Models.Badge
});

CLTApp.Models.Member = CLTApp.Model.extend({});
CLTApp.Collections.Members = CLTApp.Collection.extend({
    model: CLTApp.Models.Member
});

CLTApp.Models.Contact = CLTApp.Model.extend({});
CLTApp.Collections.Contacts = CLTApp.Collection.extend({
	model: CLTApp.Models.Contact
});
