
CLTApp.Models.Card = CLTApp.Model.extend({
});

CLTApp.Collections.Cards = CLTApp.Collection.extend({
	model: CLTApp.Models.Card
});

CLTApp.Models.CardCategory = CLTApp.Model.extend({
});

CLTApp.Collections.CardCategories = CLTApp.Collection.extend({
    model: CLTApp.Models.CardCategory
});
