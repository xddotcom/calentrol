
CLTApp = new (Backbone.View.extend({
    Version: '0.1',
    
    Models: {},
    Views: {},
    Collections: {},
    
    Templates: {},
    Pages: {},
    
    configs: {
        APIHost: "http://api.clubmeiwei.com",
        StaticHost: "http://api.clubmeiwei.com",
        MediaHost: "http://api.clubmeiwei.com"
    },
    
    start: function() {
        if (window.device && window.device.version && parseFloat(window.device.version) === 7.0) {
            $('html').addClass('iOS7');
        }
        CLTApp.initVersion();
        Backbone.history.start();
    }
}))({el: document.body});

CLTApp.initVersion = function() {
    var pVersion = localStorage.getItem('version-code') || '1.0';
    if (CLTApp.Version != pVersion) {
        localStorage.clear();
        localStorage.setItem('version-code', CLTApp.Version);
    }
};
