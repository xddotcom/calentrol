
CLTApp.View = Backbone.View.extend({
    initialize: function() {
        this.delegateFastButtons();
        if (this.initView) this.initView();
    },
    displayError: function($el, text) {
        try {
            var error = JSON.parse(text);
            for (var k in error) { $el.html(error[k]);  break; };
        } catch (e) {
            $el.html(text);
        }
    },
    bindFastButton: function(el, handler) {
        this.fastButtons = this.fastButtons || [];
        var btn = new MBP.fastButton(el.length && el.length >= 1 ? el[0] : el, handler);
        this.fastButtons.push(btn);
        return btn;
    },
    delegateFastButtons: function() {
        var EVENT_NAME = 'fastclick';
        var events = (_.isFunction(this.events) ? this.events() : this.events) || {};
        var that = this;
        function byEventName(key) {
            return key.substr(0, EVENT_NAME.length + 1) === EVENT_NAME + ' ' || key === EVENT_NAME;
        }
        function toJustSelectors(key) {
            return key.substr(EVENT_NAME.length + 1);
        }
        function toMatchingElements(selector) {
            return selector === "" ? [that.el] : that.$(selector).toArray();
        }
        function registerTrigger(element) {
            new MBP.fastButton(element, function() {
                $(element).trigger(EVENT_NAME);
            });
        }
        _.chain(events).keys().filter(byEventName).map(toJustSelectors).map(toMatchingElements).flatten().each(registerTrigger);
    }
});

CLTApp.ModelView = CLTApp.View.extend({
    initView: function() {
        if (this.model) {
            this.listenTo(this.model, 'change', this.render);
            this.listenTo(this.model, 'hide', this.remove);
        }
        if (this.initModelView) this.initModelView();
    },
    template: Mustache.compile(""),
    render: function() {
        var attrs = this.model ? this.model.toJSON() : {};
        this.$el.html(this.template(attrs));
        return this;
    }
});

CLTApp.CollectionView = CLTApp.View.extend({
    ModelView: CLTApp.ModelView,
    initView: function() {
        if (this.collection) {
            this.listenTo(this.collection, 'reset', this.addAll);
            this.listenTo(this.collection, 'add', this.addOne);
            this.listenTo(this.collection, 'remove', this.removeOne);
        }
        if (this.initCollectionView) this.initCollectionView();
    },
    removeOne: function(item) {
        item.trigger('hide');
    },
    addOne: function(item) {
        var modelView = new this.ModelView({model: item});
        this.$el.append(modelView.render().el);
    },
    addAll: function() {
        if (this.collection) {
            var $list = [];
            this.collection.forEach(function(item) {
                var modelView = new this.ModelView({model: item});
                $list.push(modelView.render().el);
            }, this);
            this.$el.html($list);
        }
    },
    render: function() {
        this.addAll();
        return this;
    }
});

CLTApp.Views.GlobalNavbar = CLTApp.View.extend({
    events: {
        'fastclick .nav-btn-home': 'onClickNavBtnHome',
        'fastclick .nav-btn-search': 'onClickNavBtnSearch',
        'fastclick .nav-btn-camera': 'openCameraOptions',
        'fastclick .nav-btn-explore': 'onClickNavBtnExplore',
        'fastclick .nav-btn-profile': 'onClickNavBtnProfile',

        'fastclick .camera-btn-take': 'onClickCameraBtnTake',
        'fastclick .camera-btn-add': 'onClickCameraBtnAdd',
        'fastclick .camera-btn-close': 'closeCameraOptions',
    },
    onClickNavBtnHome: function() { CLTApp.goTo('Home'); },
    onClickNavBtnSearch: function() { CLTApp.goTo('Search'); },
    onClickNavBtnExplore: function() { CLTApp.goTo('Search'); },
    onClickNavBtnProfile: function() { CLTApp.goTo('Profile'); },
    onClickCameraBtnTake: function() {  },
    onClickCameraBtnAdd: function() {  },
    openCameraOptions: function() {
        this.$('.camera-options').removeClass('close');
        this.$('.nav-btn-camera').addClass('active');
    },
    closeCameraOptions: function(e) {
        if (e && e.stopPropagation) e.stopPropagation();
        this.$('.camera-options').addClass('close');
        this.$('.nav-btn-camera').removeClass('active');
    }
});

CLTApp.PageView = CLTApp.View.extend({
    initView: function() {
        this.views = {};
        _.bindAll(this, 'showPage', 'go', 'refresh', 'render', 'reset', 'initScroller');
        this.$('.scroll-inner').css('min-height', (this.$('.scroll').height() + 1) + 'px');
        var $el = this.$el;
        this.$('.wrapper').on('webkitAnimationEnd', function(e) {
            if (e.originalEvent.animationName == "slideouttoleft") {
            	$el.trigger('pageClose');
            } else if (e.originalEvent.animationName == "slideinfromright") {
            	$el.trigger('pageOpen');
            }
        });
        this.navbar = new CLTApp.Views.GlobalNavbar({el: this.$('> .navbar')});
        this.navbar.render();
        if (this.initPage) this.initPage();
    },
    initScroller: function() {
        if (this.scroller == null) {
            if (this.$('.iscroll').length > 0) {
                this.scroller = new IScroll(this.$('.iscroll').selector, {
                    tap: true, tagName: /^(INPUT|TEXTAREA|SELECT)$/
                });
            }
        } else {
            this.scroller.refresh();
        }
    },
    initPageNav: function(page, collection) {
        page.fetchNext = function() {
            if (page.scroller) page.scroller.scrollTo(0, 0, 1000);
            setTimeout(function() {
                collection.fetchNext({success: function(collection, xhr, options) {
                    page.resetNavigator(collection, xhr, options);
                }});
            }, 1000);
        };
        page.fetchPrev = function() {
            if (page.scroller) page.scroller.scrollTo(0, 0, 1000);
            setTimeout(function() {
                collection.fetchPrev({success: function(collection, xhr, options) {
                    page.resetNavigator(collection, xhr, options);
                }});
            }, 1000);
        };
        page.resetNavigator = function() {
            page.$('.page-nav').toggleClass('hidden', (collection.next == null && collection.previous == null));
            page.$('.page-next').toggleClass('hidden', (collection.next == null));
            page.$('.page-prev').toggleClass('hidden', (collection.previous == null));
            if (page.scroller) page.initScroller();
        };
        page.$el.on('tap', '.page-prev', page.fetchPrev);
        page.$el.on('tap', '.page-next', page.fetchNext);
        page.listenTo(collection, 'reset', page.resetNavigator);
    },
    go: function(options) {
        this.options = options || {};
        this.reset();
        if (!this.lazy || (new Date()) - (this.lastRender || 0) > this.lazy) {
            this.lastRender = new Date();
            var render = this.render, rendered = false, pageOpen = function() {
            	if (!rendered) { rendered = true; render(); }
            };
            this.$el.one('pageOpen', pageOpen);
            setTimeout(pageOpen, 1000);
        }
        this.showPage();
    },
    refresh: function() {
        this.lastRender = new Date();
        var render = this.render, rendered = false, pageOpen = function() {
        	if (!rendered) { rendered = true; render(); }
        };
        this.$el.one('pageOpen', pageOpen);
        setTimeout(pageOpen, 1000);
        this.showPage();
    },
    reset: function() {},
    showPage: function() {
        window.scrollTo(0, 0);
        this.navbar.closeCameraOptions();
        if (this.$el && this.$el.hasClass('view-hidden')) {
            var $curPage = $('.view:not(".view-hidden")');
            var curPageClosed = false, closeCurPage = function() {
            	if (!curPageClosed) {
            		curPageClosed = true;
	            	$curPage.removeClass('view-prev');
	                $curPage.find('input').attr('readonly', true);
	            }
            };
            $curPage.addClass('view-hidden');
            $curPage.addClass('view-prev');
            $curPage.one('pageClose', closeCurPage);
            setTimeout(closeCurPage, 1000);
            
            var $nextPage = this.$el;
            var nextPageOpened = false, openNextPage = function() {
            	if (!nextPageOpened) {
            		nextPageOpened = true;
	                $nextPage.removeClass('view-next');
	                $nextPage.find('input').attr('readonly', false);
	            }
            };
            $nextPage.removeClass('view-hidden');
            $nextPage.addClass('view-next');
            $nextPage.one('pageOpen', openNextPage);
            setTimeout(openNextPage, 1000);
            
            this.initScroller();
        }
    }
});
