$(function() {
    var CardHeader = CLTApp.ModelView.extend({
        render: function() {
            var attrs = this.model ? this.model.toJSON() : {};
            this.$('.title').html(attrs.title);
            this.$('.place-name').html(attrs.place.name);
            var createdby = INSTANCE.Members.get(this.model.get('createdby'));
            this.$('.avatar').attr('src', createdby.get('avatar'));
            this.$('.theme').attr('src', 'assets/img/events/ktv.jpg');
            return this;
        }
    });
    
    var CardBody = CLTApp.ModelView.extend({
        render: function() {
            var attrs = this.model ? this.model.toJSON() : {};
            this.$('.time-text').html(attrs.time.from);
            this.$('.description p').html(attrs.description);
            return this;
        }
    });
    
    var CardMap = CLTApp.ModelView.extend({
        render: function() {
            var attrs = this.model ? this.model.toJSON() : {};
            this.$('.address').html(attrs.place.address);
            this.$('.distance').html('2.5km');
            return this;
        }
    });
    
    var ChatPanel = CLTApp.ModelView.extend({
        initModelView: function() {
            this.model.going.forEach(function(item) {
                this.listenTo(item, 'chat', function() {
                    this.openPanel(item);
                });
            }, this);
        },
        events: { 'fastclick > header': 'closePanel' },
        closePanel: function() {
            this.$el.removeClass('open');
            CLTApp.Pages.CardDetail.views.attendees.collapse();
        },
        openPanel: function(member) {
            this.$el.addClass('open');
            if (member) this.$('header > h1').html(member.get('name'));
        },
        render: function() {
            var attrs = this.model ? this.model.toJSON() : {};
            this.$('header > h1').html('聊天');
            return this;
        }
    });
    
    var Attendees = CLTApp.CollectionView.extend({
        ModelView: CLTApp.ModelView.extend({
            className: 'attendee-item',
            template: Mustache.compile('<img src="{{{avatar}}}">'),
            events: { 'fastclick': 'chatWith' },
            chatWith: function() { this.model.trigger('chat'); },
        }),
        events: { 'fastclick': 'expand' },
        expand: function() {
            if (!this.$el.hasClass('expand')) {
                this.$el.addClass('expand');
                this.collection.at(0).trigger('chat');
            }
        },
        collapse: function() { this.$el.removeClass('expand'); },
        render: function() {
            this.addAll();
            return this;
        }
    });
    
    CLTApp.Pages.CardDetail = new (CLTApp.PageView.extend({
        events: {
            'fastclick .nav-btn-decline': 'onClickNavBtnDecline',
        },
        onClickNavBtnDecline: function() {
            CLTApp.goBack();
        },
        initPage: function() {
            this.card = new CLTApp.Models.Card();
            this.card.going = INSTANCE.Members;
            this.views = {
                header: new CardHeader({ model: this.card, el: this.$('.scroll-inner > header') }),
                body: new CardBody({ model: this.card, el: this.$('.scroll-inner > .detail') }),
                map: new CardMap({ model: this.card, el: this.$('.scroll-inner > .place') }),
                chatPanel: new ChatPanel({ model: this.card, el: this.$('.chat-panel') }),
                attendees: new Attendees({ collection: this.card.going, el: this.$('.attendees') })
            };
        },
    	render: function() {
    	    if (this.options.card) {
    	       this.card.set(this.options.card);
    	    } else if (this.options.cardId) {
    	        this.card.set(INSTANCE.Cards.get(this.options.cardId).toJSON());
    	    }
    	    this.initScroller();
    	    var wrapperHeight = this.$('.wrapper').height();
    	    this.$('.chat-panel').css('-webkit-transform', 'translate3d(0, ' + (wrapperHeight + 60) + 'px, 0)');
    	    this.views.chatPanel.render();
    	    this.views.attendees.render();
    	}
    }))({el: $("#view-card-detail")});
});
