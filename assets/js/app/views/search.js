$(function() {
    var CardItem = CLTApp.ModelView.extend({
        template: CLTApp.Templates['card-list-item'],
        className: 'card-list-item',
        events: { 'tap': 'viewCard' },
        viewCard: function() {
            CLTApp.goTo('CardDetail', {
                card: this.model.toJSON()
            });
        }
    })
    
    var CardItems = CLTApp.CollectionView.extend({
    	ModelView: CardItem
    });
    
    var Filters = CLTApp.CollectionView.extend({
        ModelView: CLTApp.ModelView.extend({
            tagName: 'span',
            className: "filter-item",
            template: Mustache.compile(''),
            render: function() {
                this.$el.html('<span class="fa fa-cutlery"></span>');
                this.$el.append($('<p></p>').html(this.model.get('name')));
                this.$('> span').attr('data-type', this.model.id);
                return this;
            }
        })
    });
    
    CLTApp.Pages.Search = new (CLTApp.PageView.extend({
        events: {
            'fastclick .nav-btn-left': 'returnToHome',
            'fastclick .navbar-text, .nav-btn-right': 'openFilters',
            'fastclick .filter-panel': 'closeFilters'
        },
        returnToHome: function() { CLTApp.goTo('Home'); },
        openFilters: function() { this.$('.filter-panel').removeClass('closed'); },
        closeFilters: function() { this.$('.filter-panel').addClass('closed'); },
        initPage: function() {
            this.cards = INSTANCE.Cards;
            this.cards.forEach(function(item, index) {
                var member = INSTANCE.Members.get(item.get('createdby'));
                item.set('avatar', member.get('avatar'));
            }, this);
            this.filters = INSTANCE.CardCategories;
            this.views = {
                cardItems: new CardItems({ el: this.$('.card-list'), collection: this.cards }),
                filters: new Filters({ el: this.$('.filter-panel'), collection: this.filters })
            }
        },
    	render: function() {
    	    this.views.cardItems.render();
    	    this.views.filters.render();
    	    this.initScroller();
    	}
    }))({el: $("#view-search")});
});
