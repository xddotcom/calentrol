$(function() {
    var formatTimeStr = {
        months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        days: ['Sunday', 'Monday', 'Tuesday', 'Wedensday', 'Thursday', 'Friday', 'Saturday']
    };
    var StoryList = CLTApp.CollectionView.extend({
        ModelView: CLTApp.ModelView.extend({
            events: { 'tap': 'goToCard' },
            className: 'story-item-fullscreen',
            template: CLTApp.Templates['story-item-fullscreen'],
            goToCard: function() {
                CLTApp.goTo('CardDetail', {cardId: this.model.id});
            },
            render: function() {
                var attrs = this.model ? this.model.toJSON() : {};
                attrs.coverphoto = attrs.photos[0];
                attrs.photos = attrs.photos.slice(1, 4);
                var date = attrs.time.from;
                attrs.time.month = formatTimeStr.months[date.getMonth()];
                attrs.time.day = formatTimeStr.days[date.getDay()];
                attrs.time.year = date.getFullYear();
                attrs.time.date = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
                this.$el.html(this.template(attrs));
                return this;
            }
        })
    });
    CLTApp.Pages.Home = new (CLTApp.PageView.extend({
        events: {
            'fastclick .nav-btn-search': 'onClickNavBtnSearch',
            'fastclick .nav-btn-profile': 'onClickNavBtnProfile',
            'fastclick .nav-btn-year': 'onClickNavBtnYear',
            'fastclick .nav-btn-month': 'onClickNavBtnMonth',
            'fastclick .nav-btn-week': 'onClickNavBtnWeek',
            'fastclick .nav-btn-day': 'onClickNavBtnDay'
        },
        onClickNavBtnYear: function() {
            this.$('.year-list').addClass('active').siblings().removeClass('active');
        },
        initPage: function() {
            this.$('.scroll-inner').css('min-height', this.$('.scroll').height() + 'px');
            this.stories = new CLTApp.Collections.Cards();
            this.views = {
                stories: new StoryList({ collection: this.stories, el: this.$('.scroll-inner') })
            }
        },
    	initScroller: function() {
            if (this.scroller == null) {
                if (this.$('.iscroll').length > 0) {
                    this.scroller = new IScroll(this.$('.iscroll').selector, {
                        scrollX: true, scrollY: false, tap: true, snap: true,
                        snapStep: this.$('.iscroll').width(),
                        tagName: /^(INPUT|TEXTAREA|SELECT)$/
                    });
                }
            } else {
                this.scroller.refresh();
            }
        },
        render: function() {
            this.$('> .navbar .nav-btn-home').addClass('active');
            this.stories.reset(INSTANCE.Cards.models);
            this.initScroller();
        }
    }))({el: $("#view-home")});
});
