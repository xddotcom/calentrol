$(function() {
    
    var StoryList = CLTApp.CollectionView.extend({
        ModelView: CLTApp.ModelView.extend({
            className: 'story-item',
            template: CLTApp.Templates['story-item'],
            render: function() {
                var attrs = this.model ? this.model.toJSON() : {};
                attrs.photos = _.shuffle(attrs.photos);
                attrs.coverphoto = attrs.photos[0];
                attrs.photos = attrs.photos.slice(1, 4);
                this.$el.html(this.template(attrs));
                return this;
            }
        })
    });
    
    var BadgeList = CLTApp.CollectionView.extend({
        ModelView: CLTApp.ModelView.extend({
            className: 'badge-item',
            template: Mustache.compile('<img src="{{ picture }}"><p class="text-center">{{ title }}</p>'),
        })
    });
    
    var CouponList = CLTApp.CollectionView.extend({
        ModelView: CLTApp.ModelView.extend({
            className: 'coupon-item',
            template: Mustache.compile('<img src="{{ picture }}"><p class="text-center">{{ title }}</p>'),
        })
    });
    
    CLTApp.Pages.Profile = new (CLTApp.PageView.extend({
        events: {
            'tap .scroll-inner .nav-btn': 'togglePane'
        },
        initPage: function() {
            this.badges = new CLTApp.Collections.Badges();
            this.coupons = new CLTApp.Collections.Coupons();
            this.stories = new CLTApp.Collections.Cards();
            this.views= {
                badges: new BadgeList({ el: this.$('#profile-badge-pane'), collection: this.badges }),
                stories: new StoryList({ el: this.$('#profile-story-pane'), collection: this.stories }),
                coupons: new CouponList({ el: this.$('#profile-coupon-pane'), collection: this.coupons })
            }
        },
        togglePane: function(e) {
            var selector = $(e.currentTarget).attr('data-tab');
            $(selector).siblings().removeClass('active');
            $(selector).addClass('active');
            this.initScroller();
        },
    	render: function() {
    	    this.$('> .navbar .nav-btn-profile').addClass('active');
    	    this.badges.reset(INSTANCE.myself.get('badges'));
    	    this.coupons.reset(INSTANCE.myself.get('coupons'));
    	    this.stories.reset(INSTANCE.Cards.models);
    	    this.initScroller();
    	}
    }))({el: $("#view-profile")});
    
});
