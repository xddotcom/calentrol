INSTANCE = {};
RANDOM = {
    titles: [
        '卖袈裟卖锡杖',
        '八廓街遇见观音',
        '一人开光，全家光荣',
        '相视，成陌路',
        '告别纯真的年代',
    ],
    descriptions: [
        '我们两眼放光，只听进去那句“情愿送他”，欣喜若狂。王东则拿着手机在人人上报到。小猴天生长脸，范老版一路虔诚，老徐又有皈依之心，加上我的三寸烂舌，硬是说服了姐姐把宝贝10块钱一个卖给我们，还顺手抓了把童子口袋里的糖果，结了个善缘。',
        '事后回想起姐姐音容，真是遇见天仙了。范老版是非常开心，原来八廓街的宝贝尽是些便宜货，虽然真假难辨，家里也早就挂满了母亲上一次进藏带回家的挂饰，但说不定到离开拉萨的那天，带一大口袋宝贝离开，花费还不过百，自然还是件开心的事情。',
        '随后一行人，包括拿着手机在人人上报到的王东，大家搜刮了几十个害人的转金轮，几十串骗人的玻璃珠，和两串泡MM的假天珠，以及两条充满了怨念的保命丝巾，等等……',
        '这群不靠谱的酒肉朋友必然将离我而去。而相比之下，巴黎的酒肉们，姑且信手一弹，如结块的鼻屎一般，在气流中塑成子弹的模样，暂由他们先飞去吧。',
        '刚收拾好行李，急冲冲的跑出来，亲了下门飞的面颊。范老板就站在门口，今天是他第一次带着黑色的领结，彬彬有礼的摘下帽子，“请上车吧”。',
    ],
    places: [
        { name: '八廓街集市', address: '西藏拉萨市江苏路200号' },
    ],
    photos: ['/assets/img/events/1.jpg', '/assets/img/events/2.jpg', '/assets/img/events/3.jpg', 
             '/assets/img/events/4.jpg', '/assets/img/events/5.jpg', '/assets/img/events/6.jpg'],
    duration: function() {
        var dur = { flexible: true };
        var now = new Date();
        dur.from = new Date(now.getTime() + _.random(-60, 90) * 24 * 60 * 60 * 1000);
        dur.to = new Date(dur.from.getTime() + _.random(1, 10) * 60 * 60 * 1000);
        return dur;
    }
};

INSTANCE.Members = new CLTApp.Collections.Cards([
    {
        id: 1, name: '鬼骨孖', avatar: '/assets/img/avatars/1.jpg',
    }, {
        id: 2, name: '阿傻', avatar: '/assets/img/avatars/2.jpg',
    }, {
        id: 3, name: 'Irene', avatar: '/assets/img/avatars/3.jpg',
    }, {
        id: 4, name: 'Carey', avatar: '/assets/img/avatars/4.jpg',
    }, {
        id: 5, name: 'Emma', avatar: '/assets/img/avatars/5.jpg',
    }
]);

INSTANCE.myself = new CLTApp.Models.Member({
    id: 0,
    name: '我',
    avatar: '/assets/img/avatars/0.jpg',
    badges: [
        {picture: '/assets/img/badges/1.png', title: '酒神'},
        {picture: '/assets/img/badges/2.png', title: '女神'},
        {picture: '/assets/img/badges/3.png', title: '及时雨'},
        {picture: '/assets/img/badges/4.png', title: 'KTV麦霸'},
        {picture: '/assets/img/badges/5.png', title: '长跑冠军'},
    ],
    coupons: [
        {picture: '/assets/img/coupons/1.png', title: '大昭寺10人免费参观'},
        {picture: '/assets/img/coupons/1.png', title: '八廓街1000元抵用券'},
        {picture: '/assets/img/coupons/1.png', title: '布达拉宫对折券'},
        {picture: '/assets/img/coupons/1.png', title: '内皇宫邀请函'},
        {picture: '/assets/img/coupons/1.png', title: '雅鲁藏布江宝藏通行证'},
    ],
});

INSTANCE.CardCategories = new CLTApp.Collections.CardCategories([
    { id: 1, name: '推荐' }, { id: 2, name: '附近' }, { id: 3, name: '正在发生' },
    { id: 4, name: '唱歌' }, { id: 5, name: '聚会' }, { id: 6, name: '吃饭' },
    { id: 7, name: '运动' }, { id: 8, name: '参观' }, { id: 9, name: '其他' }
]);

INSTANCE.Cards = new CLTApp.Collections.Cards();
for (var i=0; i<20; i++) {
    var instance = {
        id: i+1,
        type: _.sample(INSTANCE.CardCategories.toJSON()).id,
        createdby: _.sample(INSTANCE.Members.toJSON()).id,
        title: _.sample(RANDOM.titles),
        description: _.sample(RANDOM.descriptions),
        time: RANDOM.duration(),
        place: _.sample(RANDOM.places),
        photos: _.sample(RANDOM.photos, 6)
    }
    INSTANCE.Cards.add(instance);
};
INSTANCE.Cards.reset(INSTANCE.Cards.sortBy(function(card){
    return card.get('time').from;
}));
